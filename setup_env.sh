#!/bin/sh
#This file is part of the openmat-testenv project.
#
#Copyright 2020 Thies Hecker
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

# this script installs the docker images for the test environment

# get couchDB image
echo "Pulling docker image for couchDB..."
docker pull couchdb

mkdir couch_data

# --------------------------------------
# create an image for the openmat web UI
# --------------------------------------
echo "Building docker image for openmatDB web UI..."
# we will need the webui, pyopenmatdb, plugin_manager.py and the plugins on the server...
cd webui
./build_image.sh
cd ..

# -----------------------------------------------------
# create an image for the openmat python plug-in server
# -----------------------------------------------------
cd py_plugin_server
./build_image.sh
cd ..

echo "Finished download of docker images."

# check if network bridge exists
echo "Checking for docker network bridge \"openmat-net\"..."
docker network inspect openmat-net
if [ $? -eq 0 ]; then
  echo "Found network bridge."
else
  echo "Network bridge not found - trying to create it..."
  docker network create openmat-net
  if [ $? -eq 0 ]; then
    echo "Successfully created network bridge."
    echo "Updating server config files to network bridge..."
    python setup_network_conf.py
  else
    echo "Error: Failed to create network bridge!"
    exit 1
  fi
fi

echo "
Run \"./start_all.sh\" to launch the test enviroment.
NOTE: This environment should only be used for testing not for production!"

exit 0
