#!/bin/python
# This file is part of the openmat-testenv project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""This script will get the IP addresses of the plugin_server containers and update the webui-config accordingly"""

import subprocess
import json

containers = [
    {'name': 'openmat_py_plugins', 'port': 5020},
    {'name': 'openmat_py_plugins2', 'port': 5030}
]
web_config_file = 'webui_conf/config.json'

with open(web_config_file, 'r') as file:
    webui_config = json.loads(file.read())

# clear plugin_server_urls
webui_config['plugin_server_urls'] = []
for container in containers:
    docker_outstr = subprocess.getoutput('docker inspect {}'.format(container['name']))
    container_info = json.loads(docker_outstr)[0]

    container_ip = container_info["NetworkSettings"]["Networks"]["openmat-net"]["IPAddress"]
    print('IP address of plug-in server \"{}\": {}'.format(container['name'], container_ip))

    webui_config['plugin_server_urls'].append("http://" + container_ip + ":" + str(container['port']))

with open(web_config_file, 'w') as file:
    file.write(json.dumps(webui_config, indent=4))
print('Plug-in server urls updated in {}'.format(web_config_file))
