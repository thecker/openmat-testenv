#!/bin/python
# This file is part of the openmat-testenv project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess
import json

"""Script to get the IP of the data base container and update the config files of the plug-in servers"""

db_container_name = 'couch-test'
configs = ['py_plugins1_conf/config/config.json', 'py_plugins2_conf/config.json']

docker_outstr = subprocess.getoutput('docker inspect {}'.format(db_container_name))

container_info = json.loads(docker_outstr)[0]

db_ip = container_info["NetworkSettings"]["Networks"]["openmat-net"]["IPAddress"]

print('data base IP address:', db_ip)

# add gateway address to plugin-server config
for config in configs:
    with open(config, 'r') as file:
        config_data = json.loads(file.read())

    config_data['db_url'] = "http://" + db_ip + ":5984"
    with open(config, 'w') as file:
        file.write(json.dumps(config_data, indent=4))
    print('DB server address updated in {}'.format(config))
