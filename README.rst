*openmatDB* test environment
============================

*openmat-testenv* is a collection of configuration files and shell scripts to setup a test environment for *openmatDB*.

*openmat-testenv* is released under Apache License Version 2.0.

To run the shell scripts a UNIX shell and the *docker* and *cURL* applications are required.
Furthermore the shell scripts will download and install docker images and further software components - e.g. python
packages. The source code of other components of the *openmatDB* environment is required as well.

See NOTICE and LICENSE file for details.

For more information about this project and *openmatDB* see: https://openmatdb.readthedocs.io/en/latest/
