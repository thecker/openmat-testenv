#!/bin/sh
#This file is part of the openmat-testenv project.
#
#Copyright 2020 Thies Hecker
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

# starts all servers on docker containers and network bridge

echo "Starting couchDB..."
./start_couch.sh

echo "Checking IP address of couchDB in openmat-net and update plug-in server configs..."
python setup_plugin_servers.py

echo "Starting openmatDB plugin-server #1 (python)..."
./start_py_plugin.sh

echo "Starting openmatDB plugin-server #2 (python)..."
./start_py_plugin2.sh

echo "Checking plug-in server IP addresses and update web UI config..."
python setup_webui.py

echo "Starting openmatDB Web UI..."
# starts the web UI in interactive mode
./start_webui.sh -i

# stop all containers, when Eve is terminated
echo "Stopping docker containers..."
docker stop couch-test
docker stop openmat_py_plugins
docker stop openmat_py_plugins2
if [ $? -eq 0 ]; then
  echo "All containers stopped successfully."
else
  echo "Error: Failed to stop docker containers!"
  exit 1
fi

exit 0

