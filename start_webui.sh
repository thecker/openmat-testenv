#!/bin/sh
#This file is part of the openmat-testenv project.
#
#Copyright 2020 Thies Hecker
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

# this script starts the docker container for the web UI server

if [ "$1" = "-i" ]; then
  MODE="-it"
else
  MODE="-d"
fi

docker run $MODE --rm --name openmat_webui --network openmat-net -p 5010:5010 \
-v $(pwd)/../openmat-webui:/usr/src/app/webui \
-v $(pwd)/../pyopenmatdb:/usr/src/app/pyopenmatdb \
-v $(pwd)/webui_conf:/usr/src/app/config \
 openmat-webui
